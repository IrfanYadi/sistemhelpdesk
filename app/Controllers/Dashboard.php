<?php

namespace App\Controllers;

class Dashboard extends BaseController
{
	protected $session = NULL;
	public function index()
	{
		$data['judul'] =  "Dashboard";
		echo view('dashboard/header', $data);
		echo view('dashboard/index');
		echo view('dashboard/footer');
	}

	public function logout()
	{
		$session = session();
		$session->destroy();
		session()->setFlashdata('wrongPassword', 'Anda Berhasil Logout.');
		// Redirect ke halaman awal
		return redirect()->to(base_url('login'));
	}

	//--------------------------------------------------------------------

}
