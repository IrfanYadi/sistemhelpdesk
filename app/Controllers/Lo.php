<?php
// KURNIA CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Master_model;
use App\Models\LoModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Lo extends BaseController
{

	protected $loModel;
	protected $validation;

	public function __construct()
	{
		$this->loModel = new LoModel();
		$this->validation =  \Config\Services::validation();
		$this->master = new Master_model();
	}

	public function index()
	{

		$data = [
			'controller'    	=> 'lo',
			'judul'     		=> 'lo'
		];
		$data['skpd'] = $this->master->getLihatData('tbl_skpd', '1', 1)->getResult();
		$data['user'] = $this->master->getLihatData('tbl_user', '1', 1)->getResult();
		if (session()->get('status') == 'Admin') {
			echo view('dashboard/header', $data);
			echo view('lo/lo', $data);
			echo view('dashboard/footer', $data);
		} else {
			echo view('errors/html/error_404');
		}
	}

	public function getAll()
	{
		$response = array();

		$data['data'] = array();

		$result = $this->loModel->select('*')->findAll();

		foreach ($result as $key => $value) {

			$ops = '<div class="btn-group">';
			$ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id_lo . ')"><i class="fa fa-edit"></i></button>';
			$ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id_lo . ')"><i class="fa fa-trash"></i></button>';
			$ops .= '</div>';

			$data['data'][$key] = array(
				$value->id_lo,
				$this->master->getLihatDataLangsung('tbl_user', 'id_user', $value->id_user, 'nama'),
				$this->master->getLihatDataLangsung('tbl_skpd', 'id_skpd', $value->id_skpd, 'nama_skpd'),
				$ops,
			);
		}

		return $this->response->setJSON($data);
	}

	public function getOne()
	{
		$response = array();

		$id = $this->request->getPost('id_lo');

		$data = $this->loModel->where('id_lo', $id)->first();

		return $this->response->setJSON($data);
	}

	public function add()
	{

		$response = array();

		$fields['id_lo'] = $this->request->getPost('idLo');
		$fields['id_user'] = $this->request->getPost('idUser');
		$fields['id_skpd'] = $this->request->getPost('idSkpd');




		if ($this->loModel->insert($fields)) {

			$response['success'] = true;
			$response['messages'] = 'Data has been inserted successfully';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Insertion error!';
		}

		return $this->response->setJSON($response);
	}

	public function edit()
	{

		$response = array();

		$fields['id_lo'] = $this->request->getPost('idLo');
		$fields['id_user'] = $this->request->getPost('idUser');
		$fields['id_skpd'] = $this->request->getPost('idSkpd');



		if ($this->loModel->update($fields['id_lo'], $fields)) {

			$response['success'] = true;
			$response['messages'] = 'Successfully updated';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Update error!';
		}

		return $this->response->setJSON($response);
	}

	public function remove()
	{
		$response = array();

		$id = $this->request->getPost('id_lo');

		if (!$this->validation->check($id, 'required|numeric')) {

			throw new \CodeIgniter\Exceptions\PageNotFoundException();
		} else {

			if ($this->loModel->where('id_lo', $id)->delete()) {

				$response['success'] = true;
				$response['messages'] = 'Deletion succeeded';
			} else {

				$response['success'] = false;
				$response['messages'] = 'Deletion error!';
			}
		}

		return $this->response->setJSON($response);
	}

	public function excel()
	{
		$lo = $this->loModel->findAll();

		$spreadsheet = new Spreadsheet();
		//header/nama kolom 
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'Id LO')
			->setCellValue('B1', 'Nama User')
			->setCellValue('C1', 'Nama SKPD');

		$column = 2;
		// data SKPD
		foreach ($lo as $value) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $column, $value->id_skpd)
				->setCellValue('B' . $column, $this->master->getLihatDataLangsung('tbl_user', 'id_user', $value->id_user, 'nama'))
				->setCellValue('C' . $column, $this->master->getLihatDataLangsung('tbl_skpd', 'id_skpd', $value->id_skpd, 'nama_skpd'));
			$column++;
		}
		// tulis dalam format .xlsx
		$writer = new Xlsx($spreadsheet);
		$fileName = 'Data LO';

		// Redirect hasil generate xlsx ke web client
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=' . $fileName . '.xlsx');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
}
