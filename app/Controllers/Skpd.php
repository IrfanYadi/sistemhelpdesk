<?php
// KURNIA CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Master_model;
use App\Models\SkpdModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Skpd extends BaseController
{

	protected $skpdModel;
	protected $validation;

	public function __construct()
	{
		$this->skpdModel = new SkpdModel();
		$this->validation =  \Config\Services::validation();
		$this->master = new Master_model();
	}

	public function index()
	{

		$data = [
			'controller'    	=> 'skpd',
			'judul'     		=> 'skpd'
		];
		if (session()->get('status') == 'Admin') {
			echo view('dashboard/header', $data);
			echo view('skpd/skpd', $data);
			echo view('dashboard/footer', $data);
		} else {
			echo view('errors/html/error_404');
		}
	}

	public function getAll()
	{
		$response = array();

		$data['data'] = array();

		$result = $this->skpdModel->select('*')->findAll();

		foreach ($result as $key => $value) {

			$ops = '<div class="btn-group">';
			$ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id_skpd . ')"><i class="fa fa-edit"></i></button>';
			$ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id_skpd . ')"><i class="fa fa-trash"></i></button>';
			$ops .= '</div>';

			$data['data'][$key] = array(
				$value->id_skpd,
				$value->nama_skpd,

				$ops,
			);
		}

		return $this->response->setJSON($data);
	}

	public function getOne()
	{
		$response = array();

		$id = $this->request->getPost('id_skpd');

		$data = $this->skpdModel->where('id_skpd', $id)->first();

		return $this->response->setJSON($data);
	}

	public function add()
	{

		$response = array();

		$fields['id_skpd'] = $this->request->getPost('idSkpd');
		$fields['nama_skpd'] = $this->request->getPost('namaSkpd');




		if ($this->skpdModel->insert($fields)) {

			$response['success'] = true;
			$response['messages'] = 'Data has been inserted successfully';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Insertion error!';
		}

		return $this->response->setJSON($response);
	}

	public function edit()
	{

		$response = array();

		$fields['id_skpd'] = $this->request->getPost('idSkpd');
		$fields['nama_skpd'] = $this->request->getPost('namaSkpd');



		if ($this->skpdModel->update($fields['id_skpd'], $fields)) {

			$response['success'] = true;
			$response['messages'] = 'Successfully updated';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Update error!';
		}

		return $this->response->setJSON($response);
	}

	public function remove()
	{
		$response = array();

		$id = $this->request->getPost('id_skpd');

		if (!$this->validation->check($id, 'required|numeric')) {

			throw new \CodeIgniter\Exceptions\PageNotFoundException();
		} else {

			if ($this->skpdModel->where('id_skpd', $id)->delete()) {

				$response['success'] = true;
				$response['messages'] = 'Deletion succeeded';
			} else {

				$response['success'] = false;
				$response['messages'] = 'Deletion error!';
			}
		}

		return $this->response->setJSON($response);
	}

	public function excel()
	{
		$skpd = $this->skpdModel->findAll();

		$spreadsheet = new Spreadsheet();
		//header/nama kolom 
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'Id SKPD')
			->setCellValue('B1', 'Nama SKPD');

		$column = 2;
		// data SKPD
		foreach ($skpd as $value) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $column, $value->id_skpd)
				->setCellValue('B' . $column, $value->nama_skpd);
			$column++;
		}
		// tulis dalam format .xlsx
		$writer = new Xlsx($spreadsheet);
		$fileName = 'Data SKPD';

		// Redirect hasil generate xlsx ke web client
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=' . $fileName . '.xlsx');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
}
