<?php

namespace App\Controllers;

use App\Models\Master_model;

class Login extends BaseController
{
	protected $validation;

	public function __construct()
	{
		$this->validation =  \Config\Services::validation();
	}
	public function index()
	{
		$model = new Master_model();
		$data['judul'] = "Login";
		return view('index', $data);
	}

	public function act_login()
	{
		$model = new Master_model();
		$username = $this->request->getPost('login-username');
		$password = $this->request->getPost('login-password');
		$md5 = md5($password);
		$data = array(
			'username' => $username,
			'password' => $md5
		);
		$hasil = $model->getDataGlobal('tbl_user', $data);
		//var_dump($hasil->getResult()); exit;
		if ($hasil->countAllResults() > 0) {
			$data = $model->getLihatData('tbl_user', 'username', $username);
			//var_dump($data->getResult()); exit;
			foreach ($data->getResult() as $akun) {
				$sesi['nama'] = $akun->nama;
				$sesi['username'] = $username;
				$sesi['id_user'] = $akun->id_user;
				$sesi['status'] = $akun->status;
				$sesi['isLoggedIn'] = TRUE;
				session()->set($sesi);
				return redirect()->to(base_url('dashboard'));
			}
		} else {
			session()->setFlashdata('wrongPassword', 'Username / Password yang Anda masukan tidak sama dengan data kami.');
			// Redirect ke halaman awal
			return redirect()->to(base_url('login'));
		}
	}

	//--------------------------------------------------------------------

}
