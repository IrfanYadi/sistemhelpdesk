<?php
// KURNIA CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Master_model;
use App\Models\KeluhanModel;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Keluhan extends BaseController
{

	protected $keluhanModel;
	protected $validation;

	public function __construct()
	{
		$this->keluhanModel = new KeluhanModel();
		$this->validation =  \Config\Services::validation();
		$this->master = new Master_model();
	}

	public function index()
	{

		$data = [
			'controller'    	=> 'keluhan',
			'judul'     		=> 'keluhan'
		];
		$data['skpd'] = $this->master->getLihatData('tbl_skpd', '1', 1)->getResult();
		echo view('dashboard/header', $data);
		echo view('keluhan/keluhan', $data);
		echo view('dashboard/footer', $data);
	}

	public function getAll()
	{
		$response = array();

		$data['data'] = array();
		$id_user = session()->get('id_user');
		$result = "";
		if (session()->get('status') == 'Admin') {
			$result = $this->keluhanModel->select('*')->findAll();
		} else {
			$result = $this->keluhanModel->select('*')->where(['id_user' => $id_user])->findAll();
		}

		foreach ($result as $key => $value) {

			$ops = '<div class="btn-group">';
			if ($value->status_keluhan != 'Selesai') {
				$ops .= '	<a href="' . base_url("/proseskeluhan/index/$value->id_keluhan") . '"><button type="button" class="btn btn-sm btn-warning mr-2" ><i class="fa fa-edit">Proses</i></button></a>';
			}
			//ini untuk verifikasi
			if ($value->status_keluhan == 'Proses') {
				$ops .= '	<a href="' . base_url("/keluhan/ubahstatus/$value->id_keluhan/Selesai") . '"><button type="button" class="btn btn-sm btn-success mr-2" ><i class="fas fa-check-circle"></i> Selesai</i></button></a>';
			} else if ($value->status_keluhan == 'Selesai') {
				$ops .= '	<a href="' . base_url("/keluhan/ubahstatus/$value->id_keluhan/Proses") . '"><button type="button" class="btn btn-sm btn-danger mr-2" ><i class="fas fa-times-circle"></i> Batal Selesai</i></button></a>';
			}
			//
			$ops .= '	<button type="button" class="btn btn-sm btn-danger mr-3" onclick="remove(' . $value->id_keluhan . ')"><i class="fa fa-trash"></i></button>';
			$ops .= '</div>';

			if ($value->status_keluhan == 'Baru') {
				$status = '<span class="badge badge-pill badge-danger">' . $value->status_keluhan . '</span>';
			} else if ($value->status_keluhan == 'Proses') {
				$status = '<span class="badge badge-pill badge-warning">' . $value->status_keluhan . '</span>';
			} else {
				$status = '<span class="badge badge-pill badge-success">' . $value->status_keluhan . '</span>';
			}
			$no_keluhan = '#' . $value->no_keluhan;

			$gambar = '<img src="' . base_url('/unggahan/keluhan/' . $value->foto_keluhan . '') . '" width="150px" >';

			$data['data'][$key] = array(
				$value->id_keluhan,
				$no_keluhan,
				$value->subjek_keluhan,
				$value->keluhan,
				$value->tgl_keluhan,
				$gambar,
				$status,
				$this->master->getLihatDataLangsung('tbl_skpd', 'id_skpd', $value->id_skpd, 'nama_skpd'),
				$value->nama_pelapor,
				$value->no_hp_pelapor,
				$value->tgl_selesai,
				$value->id_user,
				$ops,
			);
		}

		return $this->response->setJSON($data);
	}

	public function getOne()
	{
		$response = array();

		$id = $this->request->getPost('id_keluhan');

		$data = $this->keluhanModel->where('id_keluhan', $id)->first();

		return $this->response->setJSON($data);
	}

	public function add()
	{

		$response = array();

		$fields['id_keluhan'] = $this->request->getPost('idKeluhan');
		$fields['no_keluhan'] = $this->request->getPost('noKeluhan');
		$fields['subjek_keluhan'] = $this->request->getPost('subjekKeluhan');
		$fields['keluhan'] = $this->request->getPost('keluhan');
		$fields['tgl_keluhan'] = $this->request->getPost('tglKeluhan');
		// $fields['foto_keluhan'] = $this->request->getPost('fotoKeluhan');
		$fields['status_keluhan'] = $this->request->getPost('statusKeluhan');
		$fields['id_skpd'] = $this->request->getPost('idSkpd');
		$fields['nama_pelapor'] = $this->request->getPost('namaPelapor');
		$fields['no_hp_pelapor'] = $this->request->getPost('noHpPelapor');
		$fields['tgl_selesai'] = date('Y-m-d');
		$fields['id_user'] = 0;

		//ini untuk foto
		if (!empty($this->request->getFile('fotoKeluhan')->getClientName())) {
			//foto 1
			$this->validation->setRules([
				'fotoKeluhan' => [
					'rules' => 'uploaded[fotoKeluhan]|mime_in[fotoKeluhan,image/png,image/jpg]|max_size[fotoKeluhan,2048]',
					'errors' => [
						'max_size' => 'Ukuran File Maksimal 2 MB'
					]
				],
			]);
			$fotoKeluhan = $this->request->getFile('fotoKeluhan');
			$fileName1 = $fotoKeluhan->getClientName();
			$temp1 = explode(".", $fileName1);
			$newfilename1 = round(microtime(true)) . '.' . end($temp1);
			$fields['foto_keluhan'] = $newfilename1;
			$fotoKeluhan->move('unggahan/keluhan', $newfilename1);
		}

		if ($this->keluhanModel->insert($fields)) {

			$response['success'] = true;
			$response['messages'] = 'Data has been inserted successfully';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Insertion error!';
		}
		return redirect()->to(base_url("keluhan/index"));
		// return $this->response->setJSON($response);
	}

	public function ubahstatus($id_keluhan, $status)
	{

		$fields['id_keluhan'] = $id_keluhan;
		$fields['status_keluhan'] = $status;

		if ($this->keluhanModel->update($fields['id_keluhan'], $fields)) {

			$response['success'] = true;
			$response['messages'] = 'Successfully updated';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Update error!';
		}

		return redirect()->to(base_url("keluhan/index"));
		// return $this->response->setJSON($response);
	}

	public function edit()
	{

		$response = array();

		$fields['id_keluhan'] = $this->request->getPost('idKeluhan');
		$fields['no_keluhan'] = $this->request->getPost('noKeluhan');
		$fields['subjek_keluhan'] = $this->request->getPost('subjekKeluhan');
		$fields['keluhan'] = $this->request->getPost('keluhan');
		$fields['tgl_keluhan'] = $this->request->getPost('tglKeluhan');
		// $fields['foto_keluhan'] = $this->request->getPost('fotoKeluhan');
		$fields['status_keluhan'] = $this->request->getPost('statusKeluhan');
		$fields['id_skpd'] = $this->request->getPost('idSkpd');
		$fields['nama_pelapor'] = $this->request->getPost('namaPelapor');
		$fields['no_hp_pelapor'] = $this->request->getPost('noHpPelapor');
		$fields['tgl_selesai'] = date('Y-m-d');
		$fields['id_user'] = 0;

		//ini untuk foto
		if (!empty($this->request->getFile('fotoKeluhan')->getClientName())) {
			//foto 1
			$this->validation->setRules([
				'fotoKeluhan' => [
					'rules' => 'uploaded[fotoKeluhan]|mime_in[fotoKeluhan,image/png,image/jpg]|max_size[fotoKeluhan,2048]',
					'errors' => [
						'max_size' => 'Ukuran File Maksimal 2 MB'
					]
				],
			]);
			$fotoKeluhan = $this->request->getFile('fotoKeluhan');
			$fileName1 = $fotoKeluhan->getClientName();
			$temp1 = explode(".", $fileName1);
			$newfilename1 = round(microtime(true)) . '.' . end($temp1);
			$fields['foto_keluhan'] = $newfilename1;
			$fotoKeluhan->move('unggahan/keluhan', $newfilename1);
		}

		if ($this->keluhanModel->update($fields['id_keluhan'], $fields)) {

			$response['success'] = true;
			$response['messages'] = 'Successfully updated';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Update error!';
		}

		return $this->response->setJSON($response);
	}

	public function remove()
	{
		$response = array();

		$id = $this->request->getPost('id_keluhan');

		if (!$this->validation->check($id, 'required|numeric')) {

			throw new \CodeIgniter\Exceptions\PageNotFoundException();
		} else {

			if ($this->keluhanModel->where('id_keluhan', $id)->delete()) {

				$response['success'] = true;
				$response['messages'] = 'Deletion succeeded';
			} else {

				$response['success'] = false;
				$response['messages'] = 'Deletion error!';
			}
		}

		return $this->response->setJSON($response);
	}

	public function excel()
	{
		$id_user = session()->get('id_user');
		$keluhan = "";
		if (session()->get('status') == 'Admin') {
			$keluhan = $this->keluhanModel->findAll();
		} else {
			$keluhan = $this->keluhanModel->where(['id_user' => $id_user])->findAll();
		}
		$spreadsheet = new Spreadsheet();
		//header/nama kolom 
		$spreadsheet->setActiveSheetIndex(0)
			->setCellValue('A1', 'Id Keluhan')
			->setCellValue('B1', 'No Keluhan')
			->setCellValue('C1', 'Subjek Keluhan')
			->setCellValue('D1', 'Keluhan')
			->setCellValue('E1', 'Tgl Keluhan')
			->setCellValue('F1', 'Status Keluhan')
			->setCellValue('G1', 'Nama SKPD')
			->setCellValue('H1', 'Nama Pelapor')
			->setCellValue('I1', 'No HP Pelapor');

		$column = 2;
		// data keluhan
		foreach ($keluhan as $value) {
			$spreadsheet->setActiveSheetIndex(0)
				->setCellValue('A' . $column, $value->id_keluhan)
				->setCellValue('B' . $column, $value->no_keluhan)
				->setCellValue('C' . $column, $value->subjek_keluhan)
				->setCellValue('D' . $column, $value->keluhan)
				->setCellValue('E' . $column, $value->tgl_keluhan)
				->setCellValue('F' . $column, $value->status_keluhan)
				->setCellValue('G' . $column, $this->master->getLihatDataLangsung('tbl_skpd', 'id_skpd', $value->id_skpd, 'nama_skpd'))
				->setCellValue('H' . $column, $value->nama_pelapor)
				->setCellValue('I' . $column, $value->no_hp_pelapor);
			$column++;
		}
		// tulis dalam format .xlsx
		$writer = new Xlsx($spreadsheet);
		$fileName = 'Data Keluhan';

		// Redirect hasil generate xlsx ke web client
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename=' . $fileName . '.xlsx');
		header('Cache-Control: max-age=0');

		$writer->save('php://output');
	}
}
