<?php
// KURNIA CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Master_model;
use App\Models\ProseskeluhanModel;
use App\Models\KeluhanModel;

class Proseskeluhan extends BaseController
{

	protected $proseskeluhanModel;
	protected $validation;

	public function __construct()
	{
		$this->proseskeluhanModel = new ProseskeluhanModel();
		$this->validation =  \Config\Services::validation();
		$this->master = new Master_model();
		$this->keluhanModel = new KeluhanModel();
	}

	public function index($id_keluhan = null)
	{
		$id_user = session()->get('id_user');
		$data = [
			'controller'    	=> 'proseskeluhan',
			'judul'     		=> 'proseskeluhan',
			'cek'				=> $this->proseskeluhanModel->select('*')->join('tbl_keluhan', 'tbl_keluhan.id_keluhan = tbl_proses_keluhan.id_keluhan')->where(['tbl_proses_keluhan.id_keluhan' => $id_keluhan, 'id_user'=> $id_user])->findAll()
		];
		if ($id_keluhan != null) {
			$data['id_keluhan'] = $id_keluhan;
		} else {
			$data['id_keluhan'] = $id_keluhan;
		}
		//untuk ganti status menjadi di proses
		$status = $this->master->getLihatDataLangsung('tbl_keluhan', 'id_keluhan', $id_keluhan, 'status_keluhan');
		if ($status == "Baru") {
			$this->ubahStatusProses($id_keluhan);
		}
		//
		echo view('dashboard/header', $data);
		echo view('proseskeluhan/proseskeluhan', $data);
		echo view('dashboard/footer', $data);
	}

	private function ubahStatusProses($id_keluhan)
	{
		$fields['status_keluhan'] = "Proses";
		$this->keluhanModel->update($id_keluhan, $fields);
		return true;
	}

	public function getAll($id_keluhan = null)
	{
		$response = array();
		$id_user = session()->get('id_user');
		$data['data'] = array();
		if ($id_keluhan == null) {
			$result = $this->proseskeluhanModel->select('*')->join('tbl_keluhan', 'tbl_keluhan.id_keluhan = tbl_proses_keluhan.id_keluhan')->where([ 'id_user'=> $id_user])->findAll();
		} else {
			$result = $this->proseskeluhanModel->select('*')->join('tbl_keluhan', 'tbl_keluhan.id_keluhan = tbl_proses_keluhan.id_keluhan')->where(['tbl_proses_keluhan.id_keluhan' => $id_keluhan, 'id_user'=> $id_user])->findAll();
		}


		foreach ($result as $key => $value) {

			$ops = '<div class="btn-group">';
			$ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id_proses_keluhan . ')"><i class="fa fa-edit"></i></button>';
			$ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id_proses_keluhan . ')"><i class="fa fa-trash"></i></button>';
			$ops .= '</div>';
			$gambar = '<img src="'.base_url('/unggahan/proseskeluhan/'.$value->foto_proses_keluhan.'').'" width="150px" >';

			$data['data'][$key] = array(
				$value->id_proses_keluhan,
				$value->id_keluhan,
				$value->tgl_proses,
				$gambar,
				$value->keterangan,

				$ops,
			);
		}

		return $this->response->setJSON($data);
	}

	public function getOne()
	{
		$response = array();

		$id = $this->request->getPost('id_proses_keluhan');

		$data = $this->proseskeluhanModel->where('id_proses_keluhan', $id)->first();

		return $this->response->setJSON($data);
	}

	public function add()
	{
		$response = array();

		$fields['id_proses_keluhan'] = $this->request->getPost('idProsesKeluhan');
		$fields['id_keluhan'] = $this->request->getPost('idKeluhan');
		$fields['tgl_proses'] = $this->request->getPost('tglProses');
		// $fields['foto_proses_keluhan'] = $this->request->getPost('fotoProsesKeluhan');
		$fields['keterangan'] = $this->request->getPost('keterangan');

		//ini untuk foto
		if (!empty($this->request->getFile('fotoProsesKeluhan')->getClientName())) {
			//foto 1
			$this->validation->setRules([
				'fotoProsesKeluhan' => [
					'rules' => 'uploaded[fotoProsesKeluhan]|mime_in[fotoProsesKeluhan,image/png,image/jpg]|max_size[fotoProsesKeluhan,2048]',
					'errors' => [
						'max_size' => 'Ukuran File Maksimal 2 MB'
					]
				],
			]);
			$fotoProsesKeluhan = $this->request->getFile('fotoProsesKeluhan');
			$fileName1 = $fotoProsesKeluhan->getClientName();
			$temp1 = explode(".", $fileName1);
			$newfilename1 = round(microtime(true)) . '.' . end($temp1);
			$fields['foto_proses_keluhan'] = $newfilename1;
			$fotoProsesKeluhan->move('unggahan/proseskeluhan', $newfilename1);
		}


		if ($this->proseskeluhanModel->insert($fields)) {

			$response['success'] = true;
			$response['messages'] = 'Data has been inserted successfully';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Insertion error!';
		}
		return redirect()->to(base_url("proseskeluhan/index"));
		// return $this->response->setJSON($response);
	}

	public function edit()
	{

		$response = array();

		$fields['id_proses_keluhan'] = $this->request->getPost('idProsesKeluhan');
		$fields['id_keluhan'] = $this->request->getPost('idKeluhan');
		$fields['tgl_proses'] = $this->request->getPost('tglProses');
		$fields['foto_proses_keluhan'] = $this->request->getPost('fotoProsesKeluhan');
		$fields['keterangan'] = $this->request->getPost('keterangan');



		if ($this->proseskeluhanModel->update($fields['id_proses_keluhan'], $fields)) {

			$response['success'] = true;
			$response['messages'] = 'Successfully updated';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Update error!';
		}

		return $this->response->setJSON($response);
	}

	public function remove()
	{
		$response = array();

		$id = $this->request->getPost('id_proses_keluhan');

		if (!$this->validation->check($id, 'required|numeric')) {

			throw new \CodeIgniter\Exceptions\PageNotFoundException();
		} else {

			if ($this->proseskeluhanModel->where('id_proses_keluhan', $id)->delete()) {

				$response['success'] = true;
				$response['messages'] = 'Deletion succeeded';
			} else {

				$response['success'] = false;
				$response['messages'] = 'Deletion error!';
			}
		}

		return $this->response->setJSON($response);
	}
}
