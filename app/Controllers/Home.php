<?php
// KURNIA CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Master_model;
use App\Models\KeluhanModel;

class Home extends BaseController
{

	protected $keluhanModel;
	protected $validation;

	public function __construct()
	{
		$this->keluhanModel = new KeluhanModel();
		$this->validation =  \Config\Services::validation();
		$this->master = new Master_model();
	}

	public function index()
	{
		$data = [
			'judul'				=> 'Beranda'
		];
		return view('home/home', $data);
	}

	public function masukankeluhan()
	{
		$data = [
			'controller'    	=> 'home',
			'judul'				=> 'Masukan Keluhan',
			'aktif'				=> 'active'
		];
		$data['skpd'] = $this->master->getLihatData('tbl_skpd', '1', 1)->getResult();
		return view('home/masukankeluhan', $data);
	}

	public function datakeluhan()
	{
		$data = [
			'controller'    	=> 'home',
			'judul'				=> 'Data Keluhan',
			'aktif'				=> 'active'
		];
		$data['skpd'] = $this->master->getLihatData('tbl_skpd', '1', 1)->getResult();
		return view('home/datakeluhan', $data);
	}
	public function cekdatakeluhan()
	{
		$data = [
			'controller'    	=> 'home',
			'judul'				=> 'Cek Data Keluhan',
			'aktif'				=> 'active',
		];
		$response = array();
		$data['data'] = array();
		$data['search'] = $this->request->getPost('search');
		return view('home/cekkeluhan', $data);
		// $result = $this->keluhanModel->select('*')->findAll();
		// foreach($result as $key => $value){
		// 	if($data['search'] == $value->no_keluhan){
		// 		return view('home/cekkeluhan', $data);
		// 	} else if($data['search'] != $value->no_keluhan) {
		// 		return redirect()->to(base_url("home/datakeluhan"));
		// 	}
		// }
	}

	public function jsondatakeluhan($search = null)
	{
		
		$data = array();
		$result = $this->keluhanModel->select('*')->where(['no_keluhan' => $search])->findAll();

		foreach ($result as $key => $value) {

			if($value->status_keluhan == 'Baru'){
				$status = '<span class="badge badge-pill badge-danger">'. $value->status_keluhan . '</span>';
			} else if($value->status_keluhan == 'Proses'){
				$status = '<span class="badge badge-pill badge-warning">'. $value->status_keluhan . '</span>';
			} else {
				$status = '<span class="badge badge-pill badge-success">' . $value->status_keluhan . '</span>';
			}

			$no_keluhan = '#' . $value->no_keluhan;
			
			$gambar = '<img src="'.base_url('/unggahan/keluhan/'.$value->foto_keluhan).'" width="150px" >';

			$data['data'][$key] = array(
				$value->id_keluhan,
				$no_keluhan,
				$value->subjek_keluhan,
				$value->keluhan,
				$value->tgl_keluhan,
				$gambar,
				$status,
				$this->master->getLihatDataLangsung('tbl_skpd', 'id_skpd', $value->id_skpd, 'nama_skpd'),
				$value->nama_pelapor,
				$value->no_hp_pelapor,
				// $value->tgl_selesai,
				$value->id_user,
			);
		}
		return $this->response->setJSON($data);
	}

	public function getOne()
	{
		$response = array();

		$id = $this->request->getPost('id_keluhan');

		$data = $this->keluhanModel->where('id_keluhan', $id)->first();

		return $this->response->setJSON($data);
	}

	public function add()
	{
		$response = array();
		$fields['id_keluhan'] = $this->request->getPost('idKeluhan');
		$fields['no_keluhan'] = $this->request->getPost('noKeluhan');
		$fields['subjek_keluhan'] = $this->request->getPost('subjekKeluhan');
		$fields['keluhan'] = $this->request->getPost('keluhan');
		$fields['tgl_keluhan'] = date('Y-m-d');
		// $fields['foto_keluhan'] = $this->request->getPost('fotoKeluhan');
		$fields['status_keluhan'] = "Baru";
		$fields['id_skpd'] = $this->request->getPost('idSkpd');
		$fields['nama_pelapor'] = $this->request->getPost('namaPelapor');
		$fields['no_hp_pelapor'] = $this->request->getPost('noHpPelapor');
		$fields['tgl_selesai'] = date('Y-m-d');
		//ambil id user LO
		$id_user = $this->master->getLihatDataLangsung('tbl_lo', 'id_skpd', $fields['id_skpd'], 'id_user');
		//
		$fields['id_user'] = ($id_user) ? $id_user : 0;

		//ini untuk foto
		if (!empty($this->request->getFile('fotoKeluhan')->getClientName())) {
			//foto 1
			$this->validation->setRules([
				'fotoKeluhan' => [
					'rules' => 'uploaded[fotoKeluhan]|mime_in[fotoKeluhan,image/png,image/jpg]|max_size[fotoKeluhan,2048]',
					'errors' => [
						'max_size' => 'Ukuran File Maksimal 2 MB'
					]
				],
			]);
			$fotoKeluhan = $this->request->getFile('fotoKeluhan');
			$fileName1 = $fotoKeluhan->getClientName();
			$temp1 = explode(".", $fileName1);
			$newfilename1 = round(microtime(true)) . '.' . end($temp1);
			$fields['foto_keluhan'] = $newfilename1;
			$fotoKeluhan->move('unggahan/keluhan', $newfilename1);
		}

		if ($this->keluhanModel->insert($fields)) {

			$response['success'] = true;
			$response['messages'] = 'Data has been inserted successfully';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Insertion error!';
		}
		return redirect()->to(base_url("home/datakeluhan"));
		// return $this->response->setJSON($response);
	}

	public function edit()
	{

		$response = array();

		$fields['id_keluhan'] = $this->request->getPost('idKeluhan');
		$fields['no_keluhan'] = $this->request->getPost('noKeluhan');
		$fields['subjek_keluhan'] = $this->request->getPost('subjekKeluhan');
		$fields['keluhan'] = $this->request->getPost('keluhan');
		$fields['tgl_keluhan'] = date('Y-m-d');
		// $fields['foto_keluhan'] = $this->request->getPost('fotoKeluhan');
		$fields['status_keluhan'] = $this->request->getPost('statusKeluhan');
		$fields['id_skpd'] = $this->request->getPost('idSkpd');
		$fields['nama_pelapor'] = $this->request->getPost('namaPelapor');
		$fields['no_hp_pelapor'] = $this->request->getPost('noHpPelapor');
		$fields['tgl_selesai'] = date('Y-m-d');
		$fields['id_user'] = 0;

		//ini untuk foto
		if (!empty($this->request->getFile('fotoKeluhan')->getClientName())) {
			//foto 1
			$this->validation->setRules([
				'fotoKeluhan' => [
					'rules' => 'uploaded[fotoKeluhan]|mime_in[fotoKeluhan,image/png,image/jpg]|max_size[fotoKeluhan,2048]',
					'errors' => [
						'max_size' => 'Ukuran File Maksimal 2 MB'
					]
				],
			]);
			$fotoKeluhan = $this->request->getFile('fotoKeluhan');
			$fileName1 = $fotoKeluhan->getClientName();
			$temp1 = explode(".", $fileName1);
			$newfilename1 = round(microtime(true)) . '.' . end($temp1);
			$fields['foto_keluhan'] = $newfilename1;
			$fotoKeluhan->move('unggahan/keluhan', $newfilename1);
		}

		if ($this->keluhanModel->update($fields['id_keluhan'], $fields)) {

			$response['success'] = true;
			$response['messages'] = 'Successfully updated';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Update error!';
		}

		return $this->response->setJSON($response);
	}

	public function remove()
	{
		$response = array();

		$id = $this->request->getPost('id_keluhan');

		if (!$this->validation->check($id, 'required|numeric')) {

			throw new \CodeIgniter\Exceptions\PageNotFoundException();
		} else {

			if ($this->keluhanModel->where('id_keluhan', $id)->delete()) {

				$response['success'] = true;
				$response['messages'] = 'Deletion succeeded';
			} else {

				$response['success'] = false;
				$response['messages'] = 'Deletion error!';
			}
		}

		return $this->response->setJSON($response);
	}
}
