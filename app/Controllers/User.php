<?php
// KURNIA CODEIGNITER 4 CRUD GENERATOR

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\Master_model;
use App\Models\UserModel;

class User extends BaseController
{

	protected $userModel;
	protected $validation;

	public function __construct()
	{
		$this->userModel = new UserModel();
		$this->validation =  \Config\Services::validation();
		$this->master = new Master_model();
	}

	public function index()
	{

		$data = [
			'controller'    	=> 'user',
			'judul'     		=> 'user'
		];

		echo view('dashboard/header', $data);
		echo view('user/user', $data);
		echo view('dashboard/footer', $data);
	}

	public function getAll()
	{
		$response = array();

		$data['data'] = array();

		$id_user = session()->get('id_user');
		$result = "";
		if (session()->get('status') == 'Admin') {
			$result = $this->userModel->select('*')->findAll();
		} else {
			$result = $this->userModel->select('*')->where(['id_user' => $id_user])->findAll();
		}

		foreach ($result as $key => $value) {

			$ops = '<div class="btn-group">';
			$ops .= '	<button type="button" class="btn btn-sm btn-info" onclick="edit(' . $value->id_user . ')"><i class="fa fa-edit"></i></button>';
			if (session()->get('status') == 'Admin'){
				$ops .= '	<button type="button" class="btn btn-sm btn-danger" onclick="remove(' . $value->id_user . ')"><i class="fa fa-trash"></i></button>';
			}
			$ops .= '</div>';

			$data['data'][$key] = array(
				$value->id_user,
				$value->username,
				$value->password,
				$value->nama,
				$value->email,
				$value->no_hp,
				$value->jabatan,
				$value->status,

				$ops,
			);
		}

		return $this->response->setJSON($data);
	}

	public function getOne()
	{
		$response = array();

		$id = $this->request->getPost('id_user');

		$data = $this->userModel->where('id_user', $id)->first();

		return $this->response->setJSON($data);
	}

	public function add()
	{

		$response = array();

		$fields['id_user'] = $this->request->getPost('idUser');
		$fields['username'] = $this->request->getPost('username');
		$fields['password'] = md5($this->request->getPost('password'));
		$fields['nama'] = $this->request->getPost('nama');
		$fields['email'] = $this->request->getPost('email');
		$fields['no_hp'] = $this->request->getPost('noHp');
		$fields['jabatan'] = $this->request->getPost('jabatan');
		$fields['status'] = $this->request->getPost('status');




		if ($this->userModel->insert($fields)) {

			$response['success'] = true;
			$response['messages'] = 'Data has been inserted successfully';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Insertion error!';
		}

		return $this->response->setJSON($response);
	}

	public function edit()
	{

		$response = array();

		$fields['id_user'] = $this->request->getPost('idUser');
		$fields['username'] = $this->request->getPost('username');
		$fields['password'] = md5($this->request->getPost('password'));
		$fields['nama'] = $this->request->getPost('nama');
		$fields['email'] = $this->request->getPost('email');
		$fields['no_hp'] = $this->request->getPost('noHp');
		$fields['jabatan'] = $this->request->getPost('jabatan');
		$fields['status'] = $this->request->getPost('status');



		if ($this->userModel->update($fields['id_user'], $fields)) {

			$response['success'] = true;
			$response['messages'] = 'Successfully updated';
		} else {

			$response['success'] = false;
			$response['messages'] = 'Update error!';
		}

		return $this->response->setJSON($response);
	}

	public function remove()
	{
		$response = array();

		$id = $this->request->getPost('id_user');

		if (!$this->validation->check($id, 'required|numeric')) {

			throw new \CodeIgniter\Exceptions\PageNotFoundException();
		} else {

			if ($this->userModel->where('id_user', $id)->delete()) {

				$response['success'] = true;
				$response['messages'] = 'Deletion succeeded';
			} else {

				$response['success'] = false;
				$response['messages'] = 'Deletion error!';
			}
		}

		return $this->response->setJSON($response);
	}
}
