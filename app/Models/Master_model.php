<?php namespace App\Models;
 //kurnia adiyoga
use CodeIgniter\Model;
 
class Master_model extends Model
{
     
    public function getData($namatabel)
    {
        $builder = $this->db->table($namatabel);
        return $builder->get();
    }
    public function getDataGlobal($namatabel, $data)
    {
        $builder = $this->db->table($namatabel);
        $builder->where($data);
        return $builder;
    }
    public function getPerintah($namatabel, $data, $where_id, $isi_where)
    {
        $builder = $this->db->table($namatabel);
        $perintah = $builder->set($data);
        $perintah =$builder->where($where_id, $isi_where);
        $perintah = $builder->getCompiledUpdate($namatabel);
        return $perintah;
    }
    public function getLihatData($nama_tabel, $kolom, $id_nya)
    {
        $builder = $this->db->table($nama_tabel);
        $builder->select('*');
        $builder->Where([$kolom => $id_nya]);
        return $builder->get();
    }
 
    public function tambahData($nama_tabel, $data){
        $query = $this->db->table($nama_tabel)->insert($data);
        return $query;
    }

    public function ubahData($nama_tabel, $data, $kolom, $id_nya)
    {
        $query = $this->db->table($nama_tabel)->update($data, array($kolom => $id_nya));
        return $query;
    }
 
    public function hapusData($nama_tabel, $kolom, $id_nya)
    {
        $query = $this->db->table($nama_tabel)->delete(array($kolom => $id_nya));
        return $query;
    } 
    public function getLihatDataLangsung($nama_tabel, $kolom, $id_nya, $namakolom)
    {
        $builder = $this->db->table($nama_tabel);
        $builder->select('*');
        $builder->where([$kolom => $id_nya]);
        $builder->orderBy($kolom, 'DESC');
        $hasil =  $builder->get();
        foreach($hasil->getResult() as $result){
            return $result->$namakolom;
        }
    }
    public function getHitungDataLangsung($nama_tabel, $kolom, $id_nya, $namakolom)
    {
        $builder = $this->db->table($nama_tabel);
        $builder->selectcount($namakolom);
        $builder->where([$kolom => $id_nya]);
        $hasil =  $builder->get();
        foreach($hasil->getResult() as $result){
            return $this->buatAngka($result->$namakolom);
        }
    }
    public function getHitungDataLangsungArray($nama_tabel, $array, $namakolom)
    {
        $builder = $this->db->table($nama_tabel);
        $builder->selectcount($namakolom);
        $builder->where($array);
        $hasil =  $builder->get();
        foreach($hasil->getResult() as $result){
            return $this->buatAngka($result->$namakolom);
        }
    }
    public function getLihatDataLangsungArray($nama_tabel, $array)
    {
        $builder = $this->db->table($nama_tabel);
        $builder->select('*');
        $builder->where($array);
        $hasil =  $builder->get();
        // foreach($hasil->getResult() as $result){
        //     return $this->buatAngka($result->$namakolom);
        // }
        return $hasil->getResult();
    }
    public function buatAngka($angka){
        // $hasil =  number_format($angka,0,',','.');
        return $angka;
    }


    public function getJumlahDataLangsung($nama_tabel, $kolom, $id_nya, $namakolom)
    {
        $builder = $this->db->table($nama_tabel);
        $builder->selectsum($namakolom);
        $builder->where([$kolom => $id_nya]);
        $hasil =  $builder->get();
        foreach($hasil->getResult() as $result){
            return $result->$namakolom;
        }
    }
    public function penyebut($nilai) {
        $nilai = abs($nilai);
        $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " ". $huruf[$nilai];
        } else if ($nilai <20) {
            $temp = $this->penyebut($nilai - 10). " belas";
        } else if ($nilai < 100) {
            $temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " seratus" . $this->penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " seribu" . $this->penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
        }     
        return $temp;
    }
    
    public function terbilang($nilai) {
        if($nilai<0) {
            $hasil = "minus ". trim($this->penyebut($nilai));
        } else {
            $hasil = trim($this->penyebut($nilai));
        }     		
        return $hasil;
    }
    public function cuaca(){
        $city ="Pesawaran";
        $key = "d5441e55ba8d2b253beb8ba4f9fbc0ae";
        $url = "https://api.openweathermap.org/data/2.5/forecast?q=$city,id&lang=id&appid=$key";

        $contents = file_get_contents($url);
        $clima = json_decode($contents, true);

        foreach($clima['list'] as $data) {
            return $data['weather'][0]['description'];
        }
    }
    public function getMySQLVersion() { 
        $output = shell_exec('mysql -V'); 
        preg_match('@[0-9]+\.[0-9]+\.[0-9]+@', $output, $version); 
        return $version[0]; 
      }
 
      function last_update( $branch='master' ) {
        $fname = FCPATH.'../.git/refs/heads/master';
        $time = filemtime($fname);
        if($time != 0 ){
            return date("Y-m-d H:i:s", $time);
          }else{
              return  "time=0";
        }
  }
}
