<?php
// ADEL CODEIGNITER 4 CRUD GENERATOR

namespace App\Models;
use CodeIgniter\Model;

class KeluhanModel extends Model {
    
	protected $table = 'tbl_keluhan';
	protected $primaryKey = 'id_keluhan';
	protected $returnType = 'object';
	protected $useSoftDeletes = false;
	protected $allowedFields = ['no_keluhan', 'subjek_keluhan', 'keluhan', 'tgl_keluhan', 'foto_keluhan', 'status_keluhan', 'id_skpd', 'nama_pelapor', 'no_hp_pelapor', 'tgl_selesai', 'id_user'];
	protected $useTimestamps = false;
	protected $createdField  = 'created_at';
	protected $updatedField  = 'updated_at';
	protected $deletedField  = 'deleted_at';
	protected $validationRules    = [];
	protected $validationMessages = [];
	protected $skipValidation     = true;    
	
}