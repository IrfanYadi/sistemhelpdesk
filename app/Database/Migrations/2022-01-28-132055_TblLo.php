<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TblLo extends Migration
{
    public function up()
    {
        //
        $this->forge->addField([
            'id_lo'           => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'id_user'           => [
                'type'           => 'INT',
                'constraint'     => 11,
            ],
            'id_skpd'           => [
                'type'           => 'INT',
                'constraint'     => 11,
            ],


        ]);
        $this->forge->addKey('id_lo', TRUE);
        $this->forge->createTable('tbl_lo');
    }

    public function down()
    {
        //
    }
}
