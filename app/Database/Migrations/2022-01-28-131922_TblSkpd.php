<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TblSkpd extends Migration
{
    public function up()
    {
        //
        $this->forge->addField([
            'id_skpd'           => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'nama_skpd'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],


        ]);
        $this->forge->addKey('id_skpd', TRUE);
        $this->forge->createTable('tbl_skpd');
    }

    public function down()
    {
        //
    }
}
