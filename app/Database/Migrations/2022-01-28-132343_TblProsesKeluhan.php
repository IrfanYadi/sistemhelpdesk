<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TblProsesKeluhan extends Migration
{
    public function up()
    {
        //
        $this->forge->addField([
            'id_proses_keluhan'  => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'id_keluhan'         => [
                'type'           => 'INT',
                'constraint'     => 11,
            ],
            'tgl_proses'         => [
                'type'           => 'DATE',
            ],
            'foto_proses_keluhan' => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'keterangan'         => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],


        ]);
        $this->forge->addKey('id_proses_keluhan', TRUE);
        $this->forge->createTable('tbl_proses_keluhan');
    }

    public function down()
    {
        //
    }
}
