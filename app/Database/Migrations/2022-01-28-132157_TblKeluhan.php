<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TblKeluhan extends Migration
{
    public function up()
    {
        //
        $this->forge->addField([
            'id_keluhan'         => [
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => TRUE,
                'auto_increment' => TRUE
            ],
            'no_keluhan'         => [
                'type'           => 'VARCHAR',
                'constraint'     => '15',
                'unique'         => true
            ],
            'subjek_keluhan'     => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'keluhan'            => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'tgl_keluhan'        => [
                'type'           => 'DATE',
            ],
            'foto_keluhan'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'status_keluhan'     => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'id_skpd'            => [
                'type'           => 'INT',
                'constraint'     => 11,
            ],
            'nama_pelapor'       => [
                'type'           => 'VARCHAR',
                'constraint'     => '255',
            ],
            'no_hp_pelapor'      => [
                'type'           => 'VARCHAR',
                'constraint'     => '15',
            ],
            'tgl_selesai'        => [
                'type'           => 'DATE',
            ],
            'id_user'            => [
                'type'           => 'INT',
                'constraint'     => 11,
            ],


        ]);
        $this->forge->addKey('id_keluhan', TRUE);
        $this->forge->createTable('tbl_keluhan');
    }

    public function down()
    {
        //
    }
}
