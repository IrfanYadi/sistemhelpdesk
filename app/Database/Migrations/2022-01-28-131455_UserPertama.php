<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class UserPertama extends Migration
{
	public function up()
	{
		//
		$data = [
			'id_user'  => '1',
			'username' => 'admin',
			'password' => md5('123456'),
			'nama'     => 'Admin',
			'email'    => 'admin@gmail.com',
			'no_hp'    => '081234567891',
			'jabatan'  => 'Admin',
			'status'   => 'Admin'
		];
		// Using Query Builder
		$this->db->table('tbl_user')->insert($data);
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
