<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class TblUser extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id_user'           => [
				'type'           => 'INT',
				'constraint'     => 11,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			],
			'username'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
			'password'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],
			'nama'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
				'NULL'			=> true
			],
			'email'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
				'NULL'			=> true
			],
			'no_hp'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
				'NULL'			=> true
			],
			'jabatan'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
				'NULL'			=> true
			],
			'status'       => [
				'type'           => 'VARCHAR',
				'constraint'     => '255',
			],


		]);
		$this->forge->addKey('id_user', TRUE);
		$this->forge->createTable('tbl_user');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		//
	}
}
