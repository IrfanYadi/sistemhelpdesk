<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $judul ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="<?= base_url('assets') ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets') ?>/dist/css/adminlte.min.css">
</head>

<body class="hold-transition login-page">
  <div class="login-box">
    <!-- /.login-logo -->
    <div class="card card-outline card-primary">
      <div class="card-header text-center">
        <a href="<?= base_url("login") ?>" class="h1"><b>Sistem Helpdesk</b></a>
      </div>
      <div class="card-body">
        <p class="login-box-msg">Silakan masukan username dan password Anda.</p>

        <form class="js-validation-signin" action="<?= base_url("login/act_login") ?>" method="POST">
          <div class="py-3">
            <div class="form-group">
              <input type="text" class="form-control form-control-alt form-control-lg" id="login-username" name="login-username" placeholder="Username" autocomplete="off">
            </div>
            <div class="form-group">
              <input type="password" class="form-control form-control-alt form-control-lg" id="login-password" name="login-password" placeholder="Password">
            </div>

            <div class="form-group row">
              <div class="col-md-6 col-xl-5">
                <button type="submit" class="btn btn-block btn-primary">
                  <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Masuk
                </button>
              </div>
              <br>
            </div>
        </form>

        <hr>
        <center>
          <p class="mb-1">
            <strong>Copyright &copy; <?= date('Y') ?> <br> <a href="https://bpkad.lampungprov.go.id/">BPKAD Provinsi Lampung</a>.</strong>
          </p>
        </center>
      </div>
      <!-- /.card-body -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.login-box -->

  <!-- jQuery -->
  <script src="<?= base_url('assets') ?>/plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url('assets') ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url('assets') ?>/dist/js/adminlte.min.js"></script>
</body>

</html>