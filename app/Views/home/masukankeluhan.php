<?php

use App\Models\Master_model;

$this->master = new Master_model();
helper('text');

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $judul ?></title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/summernote/summernote-bs4.min.css">

  <!-- jQuery -->
  <script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= base_url() ?>/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

  <!-- Font Awesome -->
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>

<body>
  <nav class="navbar sticky-top navbar-expand-lg navbar-dark" style="background-color: #BA68C8;">
    <div class="container">
      <a class="navbar-brand" href="<?= base_url("home") ?>"><i class="fas fa-headset"></i> Sistem Helpdesk</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link <?= $aktif ?>" href="<?= base_url("home/masukankeluhan") ?>">Masukan Keluhan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("home/datakeluhan") ?>">Cek Data Keluhan</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <div class="container p-5">
    <div class="card shadow mb-4">
      <div class="card-body">
        <form id="add-form" class="pl-3 pr-3" action="<?php echo base_url('home/add'); ?>" method="post" enctype="multipart/form-data">
          <input type="hidden" id="idKeluhan" name="idKeluhan" class="form-control" placeholder="Id keluhan" maxlength="11" required>
          <div class="form-group">
            <label for="noKeluhan">No keluhan</label>
            <input type="text" id="noKeluhan" name="noKeluhan" class="form-control" placeholder="No keluhan" maxlength="15" value="<?= random_string('numeric', 6) ?>" required readonly>
            <span class="text-danger">*Mohon dicatat untuk tracking keluhan</span>
          </div>
          <div class="form-group">
            <label for="subjekKeluhan">Subjek keluhan</label>
            <input type="text" id="subjekKeluhan" name="subjekKeluhan" class="form-control" placeholder="Subjek keluhan" maxlength="255" required>
          </div>
          <div class="form-group">
            <label for="keluhan">Keluhan</label>
            <input type="text" id="keluhan" name="keluhan" class="form-control" placeholder="Keluhan" maxlength="255" required>
          </div>
          <div class="form-group">
            <label for="fotoKeluhan">Foto keluhan</label>
            <input type="file" id="fotoKeluhan" name="fotoKeluhan" class="form-control" placeholder="Foto keluhan" maxlength="255" required>
          </div>
          <div class="form-group">
            <label for="idSkpd">SKPD</label>
            <select class="form-control" name="idSkpd">
              <option value="" selected>-- Pilih --</option>
              <?php foreach ($skpd as $s) { ?>
                <option value="<?= $s->id_skpd ?>"><?= $s->nama_skpd; ?></option>
              <?php } ?>
            </select>
          </div>

          <div class="form-group">
            <label for="namaPelapor"> Nama pelapor</label>
            <input type="text" id="namaPelapor" name="namaPelapor" class="form-control" placeholder="Nama pelapor" maxlength="255" required>
          </div>
          <div class="form-group">
            <label for="noHpPelapor"> No hp pelapor</label>
            <input type="text" id="noHpPelapor" name="noHpPelapor" class="form-control" placeholder="No hp pelapor" maxlength="15" required>
          </div>
          <div class="form-group">
            <div class="btn-group">
              <button type="submit" class="btn btn-success" id="add-form-btn">Submit</button>
              <button type="reset" class="btn btn-danger ml-2">Reset</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer class="text-center text-white text-lg-left" style="background-color: #BA68C8;">
    <!-- Section: Links  -->
    <section class="p-3">
      <div class="container text-center text-md-left mt-5">
        <!-- Grid row -->
        <div class="row mt-3">
          <!-- Grid column -->
          <div class="col-lg-3 mx-auto mb-4">
            <picture>
              <source media="(min-width: 360px) and (max-width: 576px)" srcset="<?= base_url() ?>/assets/dist/img/bpkad.png" width="335">
              <img src="<?= base_url() ?>/assets/dist/img/bpkad.png" alt="BPKAD Provinsi Lampung" width="450">
            </picture>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-lg-3 mx-auto mb-md-0 mb-4">
            <!-- Links -->
            <h6 class="text-uppercase font-weight-bold mb-4">
              Hubungi Kami
            </h6>
            <p><i class="fas fa-home mr-2"></i> Jl. Wolter Monginsidi No. 69. Teluk Betung Bandar Lampung kode pos 35215</p>
            <p>
              <i class="fas fa-envelope mr-2"></i>
              bpkadlampung@gmail.com
            </p>
            <p><i class="fas fa-phone mr-2"></i>(0721) 481 166</p>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
      </div>
    </section>
    <!-- Section: Links  -->

    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.2);">
      © 2022 Copyright:
      <a class="text-reset font-weight-bold" href="https://bpkad.lampungprov.go.id/">BPKAD Provinsi Lampung</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->

<!-- jQuery -->
<script src="https://adminlte.io/themes/v3/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="https://adminlte.io/themes/v3/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="https://adminlte.io/themes/v3/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/v3/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="https://adminlte.io/themes/v3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="https://adminlte.io/themes/v3/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="https://adminlte.io/themes/v3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- SweetAlert2 -->
<script src="https://adminlte.io/themes/v3/plugins/sweetalert2/sweetalert2.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="<?= base_url() ?>/assets/plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="<?= base_url() ?>/assets/plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="<?= base_url() ?>/assets/plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="<?= base_url() ?>/assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?= base_url() ?>/assets/plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="<?= base_url() ?>/assets/plugins/moment/moment.min.js"></script>
  <script src="<?= base_url() ?>/assets/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?= base_url() ?>/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="<?= base_url() ?>/assets/plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="<?= base_url() ?>/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url() ?>/assets/dist/js/adminlte.js"></script>
  <!-- AdminLTE for demo purposes -->
  <!-- <script src="<?= base_url() ?>/assets/dist/js/demo.js"></script> -->
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?= base_url() ?>/assets/dist/js/pages/dashboard.js"></script>
  <script>
    function add() {
      // reset the form 
      $("#add-form")[0].reset();
      $(".form-control").removeClass('is-invalid').removeClass('is-valid');
      // submit the add from 
      $.validator.setDefaults({
        highlight: function(element) {
          $(element).addClass('is-invalid').removeClass('is-valid');
        },
        unhighlight: function(element) {
          $(element).removeClass('is-invalid').addClass('is-valid');
        },
        errorElement: 'div ',
        errorClass: 'invalid-feedback',
        errorPlacement: function(error, element) {
          if (element.parent('.input-group').length) {
            error.insertAfter(element.parent());
          } else if ($(element).is('.select')) {
            element.next().after(error);
          } else if (element.hasClass('select2')) {
            //error.insertAfter(element);
            error.insertAfter(element.next());
          } else if (element.hasClass('selectpicker')) {
            error.insertAfter(element.next());
          } else {
            error.insertAfter(element);
          }
        },

        submitHandler: function(form) {

          var form = $('#add-form');
          // remove the text-danger
          $(".text-danger").remove();

          $.ajax({
            url: '<?php echo base_url($controller . '/add') ?>',
            type: 'post',
            // data: form.serialize(), // /converting the form data into array and sending it to server
            data: formData,
            dataType: 'json',
            beforeSend: function() {
              $('#add-form-btn').html('<i class="fa fa-spinner fa-spin"></i>');
            },
            success: function(response) {

              if (response.success === true) {

                Swal.fire({
                  position: 'bottom-end',
                  icon: 'success',
                  title: response.messages,
                  showConfirmButton: false,
                  timer: 1500
                }).then(function() {
                  $('#data_table').DataTable().ajax.reload(null, false).draw(false);
                })

              } else {

                if (response.messages instanceof Object) {
                  $.each(response.messages, function(index, value) {
                    var id = $("#" + index);

                    id.closest('.form-control')
                      .removeClass('is-invalid')
                      .removeClass('is-valid')
                      .addClass(value.length > 0 ? 'is-invalid' : 'is-valid');

                    id.after(value);

                  });
                } else {
                  Swal.fire({
                    position: 'bottom-end',
                    icon: 'error',
                    title: response.messages,
                    showConfirmButton: false,
                    timer: 1500
                  })

                }
              }
              $('#add-form-btn').html('Add');
            }
          });

          return false;
        }
      });
      $('#add-form').validate();
    }
  </script>
</body>

</html>