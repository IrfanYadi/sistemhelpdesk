<?php

use App\Models\Master_model;

$this->master = new Master_model();

?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?= $judul ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?= base_url() ?>/assets/plugins/summernote/summernote-bs4.min.css">

  <!-- jQuery -->
  <script src="<?= base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?= base_url() ?>/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
</head>

<body>
  <nav class="navbar sticky-top navbar-expand-lg navbar-dark" style="background-color: #BA68C8;">
    <div class="container">
      <a class="navbar-brand" href="<?= base_url("home") ?>"><i class="fas fa-headset"></i> Sistem Helpdesk</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("home/masukankeluhan") ?>">Masukan Keluhan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?= base_url("home/datakeluhan") ?>">Cek Data Keluhan</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <br>

  <div class="container p-5">
    <div class="row">
      <div class="col-lg-6 col-md-10 col-12 d-flex flex-column justify-content-center">
        <h1 class="font-weight-bold">Selamat Datang di Sistem Helpdesk</h1>
        <h6>Sistem Helpdesk adalah sistem manajemen untuk membantu menangani kebutuhan SKPD
          terkait dengan pertanyaan, pelayanan, support teknis terhadap aplikasi atau sistem tertentu
        </h6>
        <a href="<?= base_url("home/masukankeluhan") ?>"><button type="button" class="btn text-white font-weight-bold rounded-pill" style="background-color: #BA68C8;">Ayo Masukan Keluhan Anda!</button></a>
      </div>
      <div class="col-lg-6">
        <img src="<?= base_url() ?>/assets/dist/img/contactus.gif" alt="customerservice" class="img-fluid">
      </div>
    </div>
  </div>

  <!-- Footer -->
  <footer class="text-center text-white text-lg-left" style="background-color: #BA68C8;">
    <!-- Section: Links  -->
    <section class="p-3">
      <div class="container text-center text-md-left mt-5">
        <!-- Grid row -->
        <div class="row mt-3">
          <!-- Grid column -->
          <div class="col-lg-3 mx-auto mb-4">
            <picture>
              <source media="(min-width: 360px) and (max-width: 576px)" srcset="<?= base_url() ?>/assets/dist/img/bpkad.png" width="335">
              <img src="<?= base_url() ?>/assets/dist/img/bpkad.png" alt="BPKAD Provinsi Lampung" width="450">
            </picture>
          </div>
          <!-- Grid column -->

          <!-- Grid column -->
          <div class="col-lg-3 mx-auto mb-md-0 mb-4">
            <!-- Links -->
            <h6 class="text-uppercase font-weight-bold mb-4">
              Hubungi Kami
            </h6>
            <p><i class="fas fa-home mr-2"></i> Jl. Wolter Monginsidi No. 69. Teluk Betung Bandar Lampung kode pos 35215</p>
            <p>
              <i class="fas fa-envelope mr-2"></i>
              bpkadlampung@gmail.com
            </p>
            <p><i class="fas fa-phone mr-2"></i>(0721) 481 166</p>
          </div>
          <!-- Grid column -->
        </div>
        <!-- Grid row -->
      </div>
    </section>
    <!-- Section: Links  -->

    <!-- Copyright -->
    <div class="text-center p-4" style="background-color: rgba(0, 0, 0, 0.2);">
      © 2022 Copyright:
      <a class="text-reset font-weight-bold" href="https://bpkad.lampungprov.go.id/">BPKAD Provinsi Lampung</a>
    </div>
    <!-- Copyright -->
  </footer>
  <!-- Footer -->




  <!-- Bootstrap 4 -->
  <script src="<?= base_url() ?>/assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- ChartJS -->
  <script src="<?= base_url() ?>/assets/plugins/chart.js/Chart.min.js"></script>
  <!-- Sparkline -->
  <script src="<?= base_url() ?>/assets/plugins/sparklines/sparkline.js"></script>
  <!-- JQVMap -->
  <script src="<?= base_url() ?>/assets/plugins/jqvmap/jquery.vmap.min.js"></script>
  <script src="<?= base_url() ?>/assets/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="<?= base_url() ?>/assets/plugins/jquery-knob/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="<?= base_url() ?>/assets/plugins/moment/moment.min.js"></script>
  <script src="<?= base_url() ?>/assets/plugins/daterangepicker/daterangepicker.js"></script>
  <!-- Tempusdominus Bootstrap 4 -->
  <script src="<?= base_url() ?>/assets/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
  <!-- Summernote -->
  <script src="<?= base_url() ?>/assets/plugins/summernote/summernote-bs4.min.js"></script>
  <!-- overlayScrollbars -->
  <script src="<?= base_url() ?>/assets/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url() ?>/assets/dist/js/adminlte.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <script src="<?= base_url() ?>/assets/dist/js/pages/dashboard.js"></script>
</body>

</html>