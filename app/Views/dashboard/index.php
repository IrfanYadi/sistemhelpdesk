<?php

use App\Models\Master_model;

$model = new Master_model();
$id_user = session()->get('id_user');
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0">Dashboard</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-info">
            <div class="inner">
              <h3>
                <?php
                if (session()->get('status') == 'Admin') :
                  echo $this->master->getHitungDataLangsung('tbl_keluhan', 1, 1, 'id_keluhan');
                else :
                  echo $this->master->getHitungDataLangsung('tbl_keluhan', 'id_user', $id_user, 'id_keluhan');
                endif;
                ?>
              </h3>
              <p class="font-weight-bold">Jumlah Keluhan</p>
            </div>
            <div class="icon">
              <i class="fas fa-database"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-danger">
            <div class="inner">
              <h3>
                <?php
                if (session()->get('status') == 'Admin') :
                  echo $this->master->getHitungDataLangsung('tbl_keluhan', 'status_keluhan', 'Baru', 'status_keluhan');
                else :
                  echo $this->master->getHitungDataLangsungArray('tbl_keluhan', ['id_user' => $id_user, 'status_keluhan' => 'Baru'], 'status_keluhan');
                endif;
                ?>
              </h3>
              <p class="font-weight-bold">Keluhan Baru</p>
            </div>
            <div class="icon">
              <i class="fas fa-ticket-alt"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-warning">
            <div class="inner text-white">
              <h3>
                <?php
                if (session()->get('status') == 'Admin') :
                  echo $this->master->getHitungDataLangsung('tbl_keluhan', 'status_keluhan', 'Proses', 'status_keluhan');
                else :
                  echo $this->master->getHitungDataLangsungArray('tbl_keluhan', ['id_user' => $id_user, 'status_keluhan' => 'Proses'], 'status_keluhan');
                endif;
                ?>
              </h3>
              <p class="font-weight-bold">Keluhan Dalam Proses</p>
            </div>
            <div class="icon">
              <i class="fas fa-hourglass-half"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-6">
          <!-- small box -->
          <div class="small-box bg-success">
            <div class="inner">
              <h3>
                <?php
                if (session()->get('status') == 'Admin') :
                  echo $this->master->getHitungDataLangsung('tbl_keluhan', 'status_keluhan', 'Selesai', 'status_keluhan');
                else :
                  echo $this->master->getHitungDataLangsungArray('tbl_keluhan', ['id_user' => $id_user, 'status_keluhan' => 'Selesai'], 'status_keluhan');
                endif;
                ?>
              </h3>
              <p class="font-weight-bold">Keluhan Selesai</p>
            </div>
            <div class="icon">
              <i class="fas fa-check-circle"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->