<!-- Font Awesome -->
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/fontawesome-free/css/all.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<!-- SweetAlert2 -->
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
<!-- DataTables -->
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://adminlte.io/themes/v3/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">

<!-- Google Font: Source Sans Pro -->
<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<div class="container-fluid">
			<div class="row mb-2">
				<div class="col-sm-6">
					<!-- <h1>keluhan</h1> -->
				</div>
				<div class="col-sm-6">
					<ol class="breadcrumb float-sm-right">
						<li class="breadcrumb-item"><a href="#">Home</a></li>
						<li class="breadcrumb-item active">keluhan</li>
					</ol>
				</div>
			</div>
		</div><!-- /.container-fluid -->
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">
			<div class="col-12">
				<div class="card">
					<div class="card-header">
						<div class="row">
							<div class="col-md-6 mt-2">
								<h3 class="card-title">keluhan</h3>
							</div>
							<div class="col-md-2 btn-group" role="group" aria-label="excel">
								<a href="<?= base_url('keluhan/excel') ?>" type="button" class="btn btn-info" name="excel">
									<b>
										<i class="fas fa-file-csv"></i>&nbsp;&nbsp;Excel
									</b>
								</a>
							</div>
							<div class="col-md-4">
								<button type="button" class="btn btn-block btn-success" onclick="add()" title="Add"> <i class="fa fa-plus"></i> Tambah</button>
							</div>
						</div>
					</div>
					<!-- /.card-header -->
					<div class="card-body">
						<table id="data_table" class="table table-bordered table-striped">
							<thead>
								<tr>
									<th>Id keluhan</th>
									<th>No keluhan</th>
									<th>Subjek keluhan</th>
									<th>Keluhan</th>
									<th>Tgl keluhan</th>
									<th>Foto keluhan</th>
									<th>Status keluhan</th>
									<th>Id skpd</th>
									<th>Nama pelapor</th>
									<th>No hp pelapor</th>
									<th>Tgl selesai</th>
									<th>Id user</th>
									<th></th>
								</tr>
							</thead>
						</table>
					</div>
					<!-- /.card-body -->
				</div>
				<!-- /.card -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- Add modal content -->
	<div id="add-modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-xl">
			<div class="modal-content">
				<div class="text-center p-3" style="background-color: #BA68C8;">
					<h4 class="modal-title text-white" id="info-header-modalLabel">Tambah</h4>
				</div>
				<div class="modal-body">
					<form id="add-form" class="pl-3 pr-3" action="<?php echo base_url('keluhan/add'); ?>" method="post" enctype="multipart/form-data">
						<div class="row">
							<input type="hidden" id="idKeluhan" name="idKeluhan" class="form-control" placeholder="Id keluhan" maxlength="11" required>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="noKeluhan"> No keluhan: <span class="text-danger">*</span> </label>
									<input type="text" id="noKeluhan" name="noKeluhan" class="form-control" placeholder="No keluhan" maxlength="15" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="subjekKeluhan"> Subjek keluhan: <span class="text-danger">*</span> </label>
									<input type="text" id="subjekKeluhan" name="subjekKeluhan" class="form-control" placeholder="Subjek keluhan" maxlength="255" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="keluhan"> Keluhan: <span class="text-danger">*</span> </label>
									<input type="text" id="keluhan" name="keluhan" class="form-control" placeholder="Keluhan" maxlength="255" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="tglKeluhan"> Tgl keluhan: <span class="text-danger">*</span> </label>
									<input type="date" id="tglKeluhan" name="tglKeluhan" class="form-control" dateISO="true" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="fotoKeluhan"> Foto keluhan: <span class="text-danger">*</span> </label>
									<input type="file" id="fotoKeluhan" name="fotoKeluhan" class="form-control" placeholder="Foto keluhan" maxlength="255" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="statusKeluhan"> Status keluhan: <span class="text-danger">*</span> </label>
									<input type="text" id="statusKeluhan" name="statusKeluhan" class="form-control" placeholder="Status keluhan" maxlength="255" required>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4">
								<div class="form-group">
									<label for="idSkpd"> Id skpd: <span class="text-danger">*</span> </label>
									<select class="form-control" name="idSkpd">
										<?php foreach ($skpd as $s) { ?>
											<option value="<?= $s->id_skpd ?>"><?= $s->nama_skpd; ?></option>
										<?php } ?>
									</select>
									<!-- <input type="number" id="idSkpd" name="idSkpd" class="form-control" placeholder="Id skpd" maxlength="11" number="true" required> -->
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="namaPelapor"> Nama pelapor: <span class="text-danger">*</span> </label>
									<input type="text" id="namaPelapor" name="namaPelapor" class="form-control" placeholder="Nama pelapor" maxlength="255" required>
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<label for="noHpPelapor"> No hp pelapor: <span class="text-danger">*</span> </label>
									<input type="text" id="noHpPelapor" name="noHpPelapor" class="form-control" placeholder="No hp pelapor" maxlength="15" required>
								</div>
							</div>
						</div>
						<div class="row">

						</div>

						<div class="form-group text-center">
							<div class="btn-group">
								<button type="submit" class="btn btn-success" id="add-form-btn">Tambah</button>
								<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
							</div>
						</div>
					</form>
				</div>
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

	<!-- Control Sidebar -->
	<aside class="control-sidebar control-sidebar-dark">
		<!-- Control sidebar content goes here -->
	</aside>
	<!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="https://adminlte.io/themes/v3/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="https://adminlte.io/themes/v3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="https://adminlte.io/themes/v3/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="https://adminlte.io/themes/v3/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- DataTables -->
<script src="https://adminlte.io/themes/v3/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="https://adminlte.io/themes/v3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="https://adminlte.io/themes/v3/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="https://adminlte.io/themes/v3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<!-- SweetAlert2 -->
<script src="https://adminlte.io/themes/v3/plugins/sweetalert2/sweetalert2.min.js"></script>
<script>
	$(function() {
		$('#data_table').DataTable({
			"paging": true,
			"lengthChange": false,
			"searching": true,
			"ordering": true,
			"info": true,
			"autoWidth": false,
			"responsive": true,
			"ajax": {
				"url": '<?php echo base_url($controller . '/getAll') ?>',
				"type": "POST",
				"dataType": "json",
				async: "true"
			}
		});
	});

	function add() {
		// reset the form 
		$("#add-form")[0].reset();
		$(".form-control").removeClass('is-invalid').removeClass('is-valid');
		$('#add-modal').modal('show');
		// submit the add from 
		$.validator.setDefaults({
			highlight: function(element) {
				$(element).addClass('is-invalid').removeClass('is-valid');
			},
			unhighlight: function(element) {
				$(element).removeClass('is-invalid').addClass('is-valid');
			},
			errorElement: 'div ',
			errorClass: 'invalid-feedback',
			errorPlacement: function(error, element) {
				if (element.parent('.input-group').length) {
					error.insertAfter(element.parent());
				} else if ($(element).is('.select')) {
					element.next().after(error);
				} else if (element.hasClass('select2')) {
					//error.insertAfter(element);
					error.insertAfter(element.next());
				} else if (element.hasClass('selectpicker')) {
					error.insertAfter(element.next());
				} else {
					error.insertAfter(element);
				}
			},

			submitHandler: function(form) {

				var form = $('#add-form');
				// remove the text-danger
				$(".text-danger").remove();

				$.ajax({
					url: '<?php echo base_url($controller . '/add') ?>',
					type: 'post',
					// data: form.serialize(), // /converting the form data into array and sending it to server
					data: formData,
					dataType: 'json',
					beforeSend: function() {
						$('#add-form-btn').html('<i class="fa fa-spinner fa-spin"></i>');
					},
					success: function(response) {

						if (response.success === true) {

							Swal.fire({
								position: 'bottom-end',
								icon: 'success',
								title: response.messages,
								showConfirmButton: false,
								timer: 1500
							}).then(function() {
								$('#data_table').DataTable().ajax.reload(null, false).draw(false);
								$('#add-modal').modal('hide');
							})

						} else {

							if (response.messages instanceof Object) {
								$.each(response.messages, function(index, value) {
									var id = $("#" + index);

									id.closest('.form-control')
										.removeClass('is-invalid')
										.removeClass('is-valid')
										.addClass(value.length > 0 ? 'is-invalid' : 'is-valid');

									id.after(value);

								});
							} else {
								Swal.fire({
									position: 'bottom-end',
									icon: 'error',
									title: response.messages,
									showConfirmButton: false,
									timer: 1500
								})

							}
						}
						$('#add-form-btn').html('Add');
					}
				});

				return false;
			}
		});
		$('#add-form').validate();
	}

	function remove(id_keluhan) {
		Swal.fire({
			title: 'Are you sure of the deleting process?',
			text: "You cannot back after confirmation",
			icon: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Confirm',
			cancelButtonText: 'Cancel'
		}).then((result) => {

			if (result.value) {
				$.ajax({
					url: '<?php echo base_url($controller . '/remove') ?>',
					type: 'post',
					data: {
						id_keluhan: id_keluhan
					},
					dataType: 'json',
					success: function(response) {

						if (response.success === true) {
							Swal.fire({
								position: 'bottom-end',
								icon: 'success',
								title: response.messages,
								showConfirmButton: false,
								timer: 1500
							}).then(function() {
								$('#data_table').DataTable().ajax.reload(null, false).draw(false);
							})
						} else {
							Swal.fire({
								position: 'bottom-end',
								icon: 'error',
								title: response.messages,
								showConfirmButton: false,
								timer: 1500
							})


						}
					}
				});
			}
		})
	}
</script>
</body>

</html>